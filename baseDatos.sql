-- SEQUENCE: public.prueba_id_seq

-- DROP SEQUENCE public.prueba_id_seq;

CREATE SEQUENCE public.prueba_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.prueba_id_seq
    OWNER TO postgres;
-- Table: public.prueba

-- DROP TABLE public.prueba;

CREATE TABLE public.prueba
(
    id integer NOT NULL DEFAULT nextval('prueba_id_seq'::regclass),
    nombre character varying(100) COLLATE pg_catalog."default",
    otro character varying(5) COLLATE pg_catalog."default",
    CONSTRAINT prueba_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.prueba
    OWNER to postgres;