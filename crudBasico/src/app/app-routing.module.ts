import { UpdateComponent } from './COMPONENTES/update/update.component';
import { AddComponent } from './COMPONENTES/add/add.component';
import { InicioComponent } from './COMPONENTES/inicio/inicio.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'',redirectTo:'/inicio', pathMatch:'full'},
  {path:'inicio', component:InicioComponent},
  {path:'add',component:AddComponent},
  {path:'edit/:id',component:UpdateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
