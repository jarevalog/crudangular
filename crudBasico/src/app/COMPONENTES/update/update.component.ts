import { EquipoService, Equipo } from '../../SERVICIO/equipo.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateComponent implements OnInit {
  equipo: Equipo = {
    id: '',
    nombre: '',
    otro: '',
  };

  constructor(
    private EquipoService: EquipoService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id_entrada = this.activeRoute.snapshot.params.id;
    console.log('id de entrada ' + id_entrada);
    if (id_entrada) {
      this.EquipoService.getUnEquipo(id_entrada).subscribe(
        (res) => {
          var myArray = JSON.parse(JSON.stringify(res));
          this.equipo = myArray[0];
          console.log('respuesta init modificar ' + JSON.stringify(myArray));
        },
        (err) => console.log('error ')
      );
    }
  }

  modificar() {
    console.log('objeto a modificar '+JSON.stringify(this.equipo))
    this.EquipoService.modificarEquipo(this.equipo).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => console.log('error ' +  JSON.stringify(err))
    );
    this.router.navigate(['/inicio']);
  }
}
