import { EquipoService, Equipo } from '../../SERVICIO/equipo.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  listarEquipos: Equipo[] = [];

  constructor(private EquipoService:EquipoService, private router:Router) { }

  ngOnInit(): void {
    this.listarEquipo()
  }

  listarEquipo(){
    this.EquipoService.getEquipos().subscribe(
      res=>{console.log(res)
        this.listarEquipos=<any>res;
      },err=> console.log(err)
    );
  }
eliminar(id:string){
  this.EquipoService.elminarEquipo(id).subscribe(
    res=>{
      console.log('>>>>> id '+id)
      console.log('eliminado')
      this.listarEquipo();
  },
  err=>
    console.log('error')
  )
}

modificar(id:string){
  this.router.navigate(['/edit/'+id]);
}


}
