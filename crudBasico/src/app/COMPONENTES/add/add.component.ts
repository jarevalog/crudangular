import { EquipoService, Equipo } from '../../SERVICIO/equipo.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  equipo:Equipo={
    id:"",
    nombre:"",
    otro:""
  }

  constructor(private EquipoService:EquipoService, private router:Router) { }

  ngOnInit(): void {
  }

  agregar(){
    delete this.equipo.id;
    console.log('>>>> agregando '+this.equipo.nombre)
    this.EquipoService.agregarEquipo(this.equipo).subscribe();
    this.router.navigate(['/inicio'])
  }
}
