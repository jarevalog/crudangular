const pgp = require("pg-promise")(/*options*/);
const conexion = pgp("postgres://postgres:postgres@localhost:5432/db_basico");

conexion.one("SELECT $1 AS value", 123)
    .then(function (data) {
        console.log("DATA:", data.value);
    })
    .catch(function (error) {
        console.log("ERROR:", error);
    });

module.exports = conexion