const rutas = require("express").Router()
const conexion = require("./connection")

//asignar las rutas
rutas.get("/",function(req,res){
    conexion.query("SELECT * from prueba", 123)
    .then(function (data) {
        res.json(data)
    })
    .catch(function (error) {
        res.json(error);
    });
})

rutas.get("/:id",function(req,res){
    const {id} = req.params
    conexion.query("SELECT * from prueba where id = $1", [id])
    .then(function (data) {
        res.json(data)
    })
    .catch(function (error) {
        res.json(error);
    });
})

rutas.post("/",function(req,res){
    const {nombre,otro} = req.body
    conexion.query("insert into prueba(nombre,otro) values($1,$2)", [nombre, otro])
    .then(function (data) {
        res.json({status:"agregado con exito"})
    })
    .catch(function (error) {
        res.json(error);
    });
})

rutas.delete("/:id",function(req,res){
    const {id} = req.params
    conexion.query("delete from prueba where id = $1", [id])
    .then(function (data) {
        res.json({status:"eliminado con exito"})
    })
    .catch(function (error) {
        res.json(error);
    });
})

rutas.put("/",function(req,res){
    const {id,nombre,otro} = req.body
    conexion.query("update prueba set nombre=$1, otro=$2 where id=$3", [nombre, otro,id])
    .then(function (data) {
        res.json({status:"actualizado con exito"})
    })
    .catch(function (error) {
        res.json(error);
    });
})

module.exports = rutas